import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('127.0.0.1', 8002))
sock.listen(5)
# 获取客户端socket对象
conn, address = sock.accept()
print('连接来了')
# 获取客户端的【握手】信息
data = conn.recv(1024)
print('请求数据：',data)
"""
GET / HTTP/1.1\r\n
Host: 127.0.0.1:8002\r\n
Connection: Upgrade\r\n
Pragma: no-cache\r\n
Cache-Control: no-cache\r\n
Upgrade: websocket\r\n
Origin: http://localhost:63342\r\n
Sec-WebSocket-Version: 13\r\n
User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36\r\n
Accept-Encoding: gzip, deflate, sdch, br\r\n
Accept-Language: zh-CN,zh;q=0.8\r\n
Cookie: sessionid=oz9vbpkr2ue4uf0rr3gzc98tftw85pug; csrftoken=TcUt3gDWYIOejDuyZfi6rxNyyOyYAFVuJimHwT5lmP0Vwc65xErrnwmad795BGnn\r\n
Sec-WebSocket-Key: uJrmi0Tz01pCCCfq1auoeg==\r\n
Sec-WebSocket-Extensions: permessage-deflate; client_max_window_bits\r\n\r\n'
"""
# 规则，hmac1,base64加密，+magic string



conn.send('响应【握手】信息')
conn.close()