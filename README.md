# WebSocket
[Tornado](https://github.com/cvno/tornado)原生支持WebSocket
基于Tornado,WebSocket的[匿名聊天](https://github.com/cvno/anonymous-chat)
## 建立连接 (握手)

- 客户端:浏览器(websocket包)

[示例 html code](/1.html)

```
<script type="text/javascript">
    // socket 创建一个socket连接
    var socket = new WebSocket("ws://127.0.0.1:8002");
</script>
```

- 服务端:socket

[示例 py code](/1.py)

浏览器验证服务器有没有处理WebSocket的能力,服务端按照某种加密方式,加密之后返回给客户端,浏览器再解密,如果数据一致...

请求和响应的【握手】信息需要遵循规则：

1. 从请求【握手】信息中提取 Sec-WebSocket-Key
2. 利用magic_string 和 Sec-WebSocket-Key 进行hmac1加密，再进行base64加密
3. 将加密结果响应给客户端

[加密示例code](/en.py)

加密规则:
hmac1,base64加密，+magic string
也就是WebSocket的握手信息,也叫确认信息,不是三次握手,这个更上层

```
# 请求头中的这个字段
Sec-WebSocket-Key: uJrmi0Tz01pCCCfq1auoeg==\r\n

```

[参考来源](http://www.cnblogs.com/wupeiqi/articles/6558766.html)

## 客户端和服务端收发数据 (封包/解包)

客户端和服务端传输数据时，需要对数据进行【封包】和【解包】。客户端的JavaScript类库已经封装【封包】和【解包】过程，但Socket服务端需要手动实现。

- [解包示例code](/de.py)

解包原理
><http://www.cnblogs.com/wupeiqi/articles/6558766.html>

```
info = conn.recv(8096)

payload_len = info[1] & 127
if payload_len == 126:
    extend_payload_len = info[2:4]
    mask = info[4:8]
    decoded = info[8:]
elif payload_len == 127:
    extend_payload_len = info[2:10]
    mask = info[10:14]
    decoded = info[14:]
else:
    extend_payload_len = None
    mask = info[2:6]
    decoded = info[6:]

bytes_list = bytearray()
for i in range(len(decoded)):
    chunk = decoded[i] ^ mask[i % 4]
    bytes_list.append(chunk)
body = str(bytes_list, encoding='utf-8')
print(body)
```
- [服务端封包code](/de.py)

```
def send_msg(conn, msg_bytes):
    """
    WebSocket服务端向客户端发送消息
    :param conn: 客户端连接到服务器端的socket对象,即： conn,address = socket.accept()
    :param msg_bytes: 向客户端发送的字节
    :return: 
    """
    import struct

    token = b"\x81"
    length = len(msg_bytes)
    if length < 126:
        token += struct.pack("B", length)
    elif length <= 0xFFFF:
        token += struct.pack("!BH", 126, length)
    else:
        token += struct.pack("!BQ", 127, length)

    msg = token + msg_bytes
    conn.send(msg)
    return True
```

## [demo](/demo)(chat)

- [server.py](/demo/server.py)
- [client.html](/demo/client.html)

```javascript
// 客户端(浏览器)发送信息
socket.send('发车否?')
```

断开出现:

```
Traceback (most recent call last):
  File "/Users/hyhnm/GitHub/WebSocket/demo/server.py", line 104, in <module>
    body = str(bytes_list, encoding='utf-8')
UnicodeDecodeError: 'utf-8' codec can't decode byte 0xe9 in position 1: unexpected end of data
```


